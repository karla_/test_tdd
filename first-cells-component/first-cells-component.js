class FirstCellsComponent extends Polymer.Element {

  static get is() {
    return 'first-cells-component';
  }

  static get properties() {
    return {
     
    }
  }

  sum_(a, b) {
    let total = parseFloat(a)+ parseFloat(b);
    //console.log("La suma de los número ingresados es: "+total)
    return total 
  }
  substraction_(a, b) {
    let total = parseFloat(a) - parseFloat(b);
    //console.log("La resta de los número ingresados es: "+total)
    return total 
  }
  multi(a, b) {
    let total = parseFloat(a) * parseFloat(b);
    return total 
  }
  div_(a, b) {
    let total = parseFloat(a) / parseFloat(b);
    return total 
  }

  constructor() {
    super();
  }
}

customElements.define(FirstCellsComponent.is, FirstCellsComponent);